<?php

declare(strict_types=1);

namespace Zaioll\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddNewAppCommand extends Command
{
    public function getName(): ?string
    {
        return 'zaioll:add:app';
    }

    public function getDescription(): string
    {
        return 'Add a pre configured symfony 5.4 app';
    }

    protected function configure()
    {
        $this
            ->addArgument('name', InputArgument::REQUIRED, 'The App Name')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');

        exec("cd apps && git clone --single-branch --branch master git@gitlab.com:zaioll-php/app-template.git && mv app-template {$name}");

        $password = md5(base64_encode($name));

        $this->fileEdit(
            [
                'symfony-infra_template' => "{$name}-infra",
                'symfony-app_template' => "{$name}",
                'Symfony Infrastructure Template' => ucfirst($name) . ' Infrastructure App',
                'Symfony App Template' => ucfirst($name) . ' App',
                'Zaioll\\\App' => 'Zaioll\\\\' . ucfirst($name),
                'Zaioll\\\Test\\\App' => 'Zaioll\\\\Test\\\\' . ucfirst($name),
                'src/App' => 'src/' . ucfirst($name),
                'test/App' => 'test/' . ucfirst($name),
            ],
            [
                'composer.json',
                'infra/composer.json',
            ],
            $name
        );

        $this->fileEdit(['main' => $name, 'password' => $password], 'infra/.env.dev', $name);
        $this->fileEdit(['Zaioll\\App' => 'Zaioll\\' . ucfirst($name)], ['infra/src/Kernel.php', 'infra/public/index.php'], $name);
        $this->fileEdit(
            [
                'Zaioll\\App' => 'Zaioll\\' . ucfirst($name),
                'Zaioll\\\App' => 'Zaioll\\\\' . ucfirst($name),
                'zaioll.app' => "zaioll.{$name}",
                'src/App' => 'src/' . ucfirst($name),
                'dbname: \'app\'' => "dbname: '{$name}'",
            ],
            [
                'infra/config/services.yaml',
                'infra/config/services_dev.yaml',
                'infra/config/services_test.yaml',
                'infra/config/packages/doctrine.yaml',
                'infra/config/packages/doctrine_migrations.yaml',
                'infra/config/packages/messenger.yaml',
            ],
            $name
        );
        $this->fileEdit(['Zaioll\\App' => 'Zaioll\\' . ucfirst($name)], 'infra/bin/console', $name);

        exec("cd apps/{$name} && rm -fr .git && git init && git add . && git commit -m \"Initial Commit\" && composer install && cd infra && composer install");

        $output->writeln([
            'App Creator',
            '===========',
            "apps/{$name}",
        ]);

        return Command::SUCCESS;
    }

    private function fileEdit(array $replaces, array|string $files, string $app)
    {
        $editor = function (array $replaces, string $file, string $app) {
            $file = dirname(__DIR__, 2) . "/apps/{$app}/{$file}";
            if (! file_exists($file)) {
                return false;
            }
            $contents = file_get_contents($file);
            foreach ($replaces as $search => $replace) {
                $contents = str_replace($search, $replace, $contents);
            }

            return file_put_contents($file, $contents) !== false;
        };

        if (is_array($files)) {
            foreach ($files as $file) {
                $editor($replaces, $file, $app);
            }
            return true;
        }
        return $editor($replaces, $files, $app);
    }
}
