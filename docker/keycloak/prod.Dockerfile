FROM quay.io/keycloak/keycloak-x:latest as builder

ENV KC_METRICS_ENABLED=true
ENV KC_DB=postgres
RUN /opt/keycloak/bin/kc.sh build

FROM quay.io/keycloak/keycloak-x:latest
COPY --from=builder /opt/keycloak/lib/quarkus/ /opt/keycloak/lib/quarkus/
WORKDIR /opt/keycloak

ENV KEYCLOAK_ADMIN=template
ENV KEYCLOAK_ADMIN_PASSWORD=change_me
#
RUN keytool -genkeypair -storepass password -storetype PKCS12 -keyalg RSA -keysize 2048 -dname "CN=server" -alias server -ext "SAN:c=DNS:localhost,IP:127.0.0.1" -keystore conf/server.keystore
## change these values to point to a running postgres instance
ENV KC_DB_URL=jdbc:postgresql://template-postgres/keycloak
ENV KC_DB_USERNAME=kc-template
ENV KC_DB_PASSWORD=change_me
ENV KC_HOSTNAME=localhost:8443

ENTRYPOINT ["/opt/keycloak/bin/kc.sh", "start"]