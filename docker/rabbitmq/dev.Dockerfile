FROM --platform=amd64 rabbitmq:3.9-management-alpine

# total_memory_available_override_value = 512m \
RUN echo -e "\
\ntotal_memory_available_override_value = 536870912 \
\nloopback_users.guest = false \
\nlisteners.tcp.default = 5672 \
\ndefault_pass = NDYyOTVmNDNjYTZkNjdlMzc2NTU3ODIy \
\ndefault_user = zaioll \
\nmanagement.tcp.port = 15672 \
\ncollect_statistics_interval = 60000 \
\nheartbeat = 180 \
\nlog.file.level = debug \
" >> /etc/rabbitmq/rabbitmq.conf

RUN echo -e " \
[\
  {kernel, [{net_ticktime,  120}]} \
].\
" >> /etc/rabbitmq/advanced.config