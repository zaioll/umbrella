<?php

declare(strict_types=1);

namespace Zaioll\Shared\Application\Service;

use Zaioll\Shared\Domain\Bus\Request;

/**
 * > One thing an application service definitely should not return is the complete entity.
 */
interface ApplicationServiceInterface
{
    /**
     * @return bool
     */
    public static function isTransactional(): bool;

    /**
     * @param Request $request
     * @return mixed|void
     */
    public function execute(Request $request);
}
