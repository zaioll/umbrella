<?php

declare(strict_types=1);

namespace Zaioll\Shared\Application\Service;

use Exception;

interface Session
{
    /** @throws Exception */
    public function executeOperation(callable $operation);
}
