<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Service;

use Zaioll\Shared\Application\Service\NoTransactionalSession as NoTransactional;

final class NoTransactionalSession implements NoTransactional
{
    public function executeOperation(callable $operation)
    {
        return $operation();
    }
}
