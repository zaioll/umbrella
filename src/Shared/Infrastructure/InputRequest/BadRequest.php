<?php

namespace Zaioll\Shared\Infrastructure\InputRequest;

use Exception;
use Throwable;

final class BadRequest extends Exception
{
    private $errors;

    public function __construct(array $errors, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->errors = $errors;

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return mixed[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
