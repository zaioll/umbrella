<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Symfony\InputRequest;

use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class SymfonyInputDataProcessor
{
    /**
     * @var Request|null
     */
    protected $request;

    protected $formFactory;

    protected $form;

    public function __construct(
        RequestStack $requestStack,
        FormFactoryInterface $formFactory,
        LoggerInterface $logger
    ) {
        $this->formFactory = $formFactory;

        $this->request = $requestStack->getMasterRequest();
        $logger->debug(
            sprintf(
                '%s %s%s?%s',
                $this->request->getMethod(),
                $this->request->getHost(),
                $this->request->getPathInfo(),
                $this->request->getQueryString()
            )
        );
        foreach ($this->request->headers->all() as $header => $value) {
            $value = reset($value);
            $logger->debug("$header: $value");
        }
        $logger->debug($this->request->getContent());

        $this->form = $this->extractAndValidateData($this->request);
    }

    public function getForm(): FormInterface
    {
        return $this->form;
    }

    /**
     * @param Request $request
     * @return FormInterface
     */
    abstract protected function extractAndValidateData(Request $request): FormInterface;
}
