<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Collection;

use Countable;
use ArrayIterator;
use IteratorAggregate;
use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;
use Zaioll\Shared\Infrastructure\Component\Collection\Assert;

abstract class Collection implements Countable, IteratorAggregate
{
    private $items;

    public function __construct(array $items)
    {
        $this->items = $items;

        Assert::arrayOf($this->type(), $items);
    }

    abstract protected function type(): string;

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items());
    }

    public function count(): int
    {
        return ArrayHelper::count($this->items());
    }

    protected function items(): array
    {
        return $this->items;
    }
}
