<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Redis;

use Redis;
use Exception;
use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;
use Zaioll\Shared\Infrastructure\Helper\StringHelper;

class RedisClientAdapter
{
    protected const PREFIX_KEY = '';

    private $read;

    private $write;

    public function __construct(Redis $read, Redis $write)
    {
        $this->read     = $read;
        $this->write    = $write;
    }

    public function getReadInstance(): Redis
    {
        return $this->read;
    }

    public function getWriteInstance(): Redis
    {
        return $this->write;
    }

    /**
     * @link https://github.com/phpredis/phpredis#exists
     *
     * Verify if the specified key exists.
     *
     * @param string $key
     *
     * @return int The number of keys tested that do exist.
     */
    public function exists(string $key): int
    {
        return $this->read->exists($key);
    }

    /**
     * @inheritDoc
     */
    public function prefixed(string $key): string
    {
        if (StringHelper::byteLength(static::PREFIX_KEY) > 0) {
            return static::PREFIX_KEY . ".{$key}";
        }
        return $key;
    }


    /**
     * @link https://github.com/phpredis/phpredis#scan
     *
     * @param string $keyPattern
     * @param int $count
     *
     * @return \Generator
     */
    public function scanToGenerator(string $keyPattern = '', int $count = 12000): \Generator
    {
        $it = null;
        do {
            $keys = $this->read->scan($it, $keyPattern, $count);
            if ($keys === false) {
                continue;
            }
            foreach ($keys as $valor) {
                yield $valor;
            }
        } while ($it > 0);
    }

    /**
     * @link https://github.com/phpredis/phpredis#scan
     *
     * @param string $keyPattern
     * @param int $count
     *
     * @return array|false
     */
    public function scan($keyPattern = '', int $count = 12000)
    {
        $it = null;
        $result = [];
        do {
            $keys = $this->read->scan($it, $keyPattern, $count);
            if ($keys === false) {
                continue;
            }
            foreach ($keys as $value) {
                $result[] = $value;
            }
        } while ($it > 0);
        if (ArrayHelper::count($result) > 0) {
            return $result;
        }
        return false;       
    }

    /**
     * @link https://github.com/phpredis/phpredis#keys-getkeys
     *
     * @param string $pattern
     *
     * @return array
     */
    public function keys(string $pattern): array
    {
        return $this->read->keys($pattern);
    }

    /**
     * @param \Iterator $keys
     *
     * @return bool
     */
    public function deleteGenerator(\Generator $keys): bool
    {
        try {
            if ($keys->valid()) {
                $this->del(iterator_to_array($keys, false));
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    public function publish(string $channel, string $message)
    {
        $this->write->publish($channel, $message);
    }

    /**
     * @link https://github.com/phpredis/phpredis#get
     *
     * @param string $key
     *
     * @return string|false
     */
    public function get(string $key)
    {
        if ($this->exists($key)) {
            return $this->read->get($key);
        }
        return false;
    }

    /**
     * @link https://github.com/phpredis/phpredis#set
     *
     * @param string $key
     * @param string $value
     * @param int|array $timeout Timeout or Options Array (optional). If you pass an integer, phpredis will redirect to SETEX, and will try to use Redis >= 2.6.12 extended options if you pass an array with valid values
     *
     * @return bool
     */
    public function set($key, $value, $timeout = null): bool
    {
        if ($timeout === null) {
            return $this->write->set($key, $value);
        }
        return $this->write->set($key, $value, $timeout);
    }

    /**
     * @link https://github.com/phpredis/phpredis#setex-psetex
     *
     * Set the string value in argument as value of the key, with a time to live.
     *
     * @param string $key
     * @param int $ttl
     * @param string $value
     *
     * @return bool
     */
    public function setEx(string $key, int $ttl, string $value)
    {
        return $this->write->setEx($key, $ttl, $value);
    }

    /**
     * Set the string value in argument as value of the key, with a time to live.
     * PSETEX uses a TTL in milliseconds.
     *
     * @param string $key
     * @param int $ttl
     * @param string $value
     *
     * @return bool
     */
    public function pSetEx(string $key, int $ttl, string $value)
    {
        return $this->write->setEx($key, $ttl, $value);
    }

    /**
     * @link https://github.com/phpredis/phpredis#strlen
     *
     * @param string $value
     * @return int
     */
    public function strLen(string $value)
    {
        return $this->read->strLen($value);
    }

    /**
     * Set the string value in argument as value of the key if the key doesn't already exist in the database.
     *
     * @param string $key
     * @param string $value
     *
     * @return bool
     */
    public function setNx(string $key, $value): bool
    {
        return $this->write->setNx($key, $value);
    }

    /**
     * @param string[] ...$keys
     *
     * @return int
     */
    public function del(...$keys): int
    {
        return $this->write->unlink($keys);
    }

    /**
     * Get the values of all the specified keys. If one or more keys don't exist, the array will contain FALSE at the position of the key.
     *
     * @param array $keys Array containing the list of the keyS
     *
     * @return array Array containing the values related to keys in argument
     *
     * ```php
     *  // Example
     *  $redis->set('key1', 'value1');
     *  $redis->set('key2', 'value2');
     *  $redis->set('key3', 'value3');
     *  $redis->mGet(['key1', 'key2', 'key3']); /* ['value1', 'value2', 'value3'];
     *  $redis->mGet(['key0', 'key1', 'key5']); /* [`false`, 'value1', `false`];
     * ```
     */
    public function mGet(array $keys)
    {
        return $this->read->mGet($keys);
    }

    /**
     * @link https://github.com/phpredis/phpredis#mset-msetnx
     *
     * Sets multiple key-value pairs in one atomic command. MSETNX only returns TRUE if all the keys were set (see SETNX).
     *
     * @param array $pairs
     * @return bool
     */
    public function mSet(array $pairs)
    {
        return $this->write->mSet($pairs);
    }

    /**
     * Moves a key to a different database.
     *
     * @param string $key
     * @param string $dbIndex
     *
     * @return bool
     */
    public function move(string $key, string $dbIndex): bool
    {
        return $this->write->move($key, $dbIndex);
    }

    /**
     * Sets a value and returns the previous entry at that key.
     *
     * @param string $key
     * @param string $value
     *
     * @return string
     */
    public function getSet(string $key, $value)
    {
        return $this->write->getSet($key, $value);
    }

    /**
     * Renames a key.
     *
     * @param string $srcKey
     * @param string $dstKey
     *
     * @return bool
     */
    public function rename(string $srcKey, string $dstKey): bool
    {
        return $this->write->rename($srcKey, $dstKey);
    }

    /**
     * Sets an expiration date (a timeout) on an item. pexpire requires a TTL in milliseconds.
     *
     * @param string $key
     * @param int $ttl
     *
     * @return bool
     */
    public function expire(string $key, int $ttl): bool
    {
        return $this->write->expire($key, $ttl);
    }

    /**
     * @link https://github.com/phpredis/phpredis#expireat-pexpireat
     *
     * Sets an expiration date (a timestamp) on an item. pexpireAt requires a timestamp in milliseconds.
     *
     * @param string $key
     * @param integer $timestamp
     *
     * @return bool
     */
    public function expireAt(string $key, int $timestamp): bool
    {
        return $this->write->expireAt($key, $timestamp);
    }

    /**
     * @link https://github.com/phpredis/phpredis#append
     *
     * Append specified string to the string stored in specified key.
     *
     * @param string $key
     * @param mixed $value
     *
     * @return int
     */
    public function append(string $key, $value)
    {
        return $this->write->append($key, $value);
    }

    /**
     * @link https://github.com/phpredis/phpredis#getrange
     *
     * https://github.com/phpredis/phpredis#getrange
     * 
     * @param string $key
     * @param int $start
     * @param int $end
     *
     * @return string
     */
    public function getRange(string $key, int $start, int $end): string
    {
        return $this->read->getRange($key, $start, $end);
    }

    /**
     * @link https://github.com/phpredis/phpredis#setrange
     *
     * Changes a substring of a larger string.
     *
     * @param string $key
     * @param integer $offset
     * @param mixed $value
     *
     * @return string
     */
    public function setRange(string $key, int $offset, $value)
    {
        return $this->write->setRange($key, $offset, $value);
    }

    /**
     * @link https://github.com/phpredis/phpredis#sort
     *
     * Sort the elements in a list, set or sorted set.
     *
     * @param string $key
     * @param array $options [key => value, ...] - optional, with the following keys and values:
     * ```php
     *  $options = [
     *      'by'    => 'some_pattern_*',
     *      'limit' => [0, 1],
     *      'get'   => 'some_other_pattern_*', // or an array of patterns,
     *      'sort'  => 'asc' or 'desc',
     *      'alpha' => true,
     *      'store' => 'external-key',
     *  ];
     * // Example
     *
     *  $redis->del('s');
     *  $redis->sAdd('s', 5);
     *  $redis->sAdd('s', 4);
     *  $redis->sAdd('s', 2);
     *  $redis->sAdd('s', 1);
     *  $redis->sAdd('s', 3);
     *  
     *  var_dump($redis->sort('s')); // 1,2,3,4,5
     *  var_dump($redis->sort('s', ['sort' => 'desc'])); // 5,4,3,2,1
     *  var_dump($redis->sort('s', ['sort' => 'desc', 'store' => 'out'])); // (int)5
     * ```
     *
     * @return array|int An array of values, or a number corresponding to the number of elements stored if that was used.
     */
    public function sort(string $key, array $options = [])
    {
        return $this->read->sort($key, $options);
    }

    /**
     * @link https://github.com/phpredis/phpredis#ttl-pttl
     *
     * Returns the time to live left for a given key in seconds (ttl), or milliseconds (pttl).
     *
     * @param string $key
     *
     * @return int
     */
    public function ttl(string $key)
    {
        return $this->read->ttl($key);
    }

    /**
     * @link https://github.com/phpredis/phpredis#persist
     *
     * Remove the expiration timer from a key.
     *
     * @param string $key
     * @return bool
     */
    public function persist(string $key): bool
    {
        return $this->write->persist($key);
    }
}
