<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\MySql;

use Zaioll\Shared\Infrastructure\Component\Db\Connection;
use Zaioll\Shared\Infrastructure\Component\Db\SqlDbStorage;
use Zaioll\Shared\Infrastructure\Component\Db\SqlDataTableGateway;

abstract class MySqlDataTableGateway implements SqlDataTableGateway
{
    /**
     * @var SqlDbStorage
     */
    protected $storage;

    public function __construct(SqlDbStorage $storage)
    {
        $this->storage  = $storage;
    }

    /**
     * @inheritDoc
     */
    public function getDbName(): string
    {
        return $this->getStorage()->getDbName();
    }

    /**
     * @inheritDoc
     */
    public function getConnection(): Connection
    {
        return $this->getStorage()->getConnection();
    }

    /**
     * @return SqlDbStorage
     */
    public function getStorage(): SqlDbStorage
    {
        return $this->storage;
    }

    /**
     * @param string $sql
     * @param array $params
     *
     * @return \Doctrine\DBAL\Result
     */
    public function execute(string $sql, array $params = [])
    {
        return $this->getStorage()->execute($sql, $params);
    }

    /**
     * @param string $table
     * @param array $data
     *
     * @return int|false
     */
    public function save(string $table, array $data)
    {
        return $this->getStorage()->save($table, $data, static::types($data));
    }

    /**
     * @param string $table
     * @param array $data
     *
     * @return bool
     * @throws Exception
     */
    public function update(string $table, array $data, array $criteria)
    {
        return $this->getStorage()->update($table, $data, $criteria, static::types($data));
    }

    /**
     * @param string $tabela
     * @param array $dados
     * @param array $campos
     *
     * @return int Qtd registros afetados
     */
    public function legacyInsert(string $tabela, array $dados, array $campos = [])
    {
        return $this->getStorage()->legacyInsert($tabela, $dados, $campos);
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public function positionalArgsBinds(array $params): string
    {
        return $this->getStorage()->positionalArgsBinds($params);
    }

    /**
     * @param array $state
     * @return array
     */
    abstract public static function types(array $state): array;
}
