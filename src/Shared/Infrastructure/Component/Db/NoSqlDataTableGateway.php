<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Db;

use Zaioll\Shared\Infrastructure\Component\Db\DataTableGateway;

interface NoSqlDataTableGateway extends DataTableGateway
{
}
