<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Db;

use Zaioll\Shared\Infrastructure\Component\Db\Connection;
use Zaioll\Shared\Infrastructure\Component\Db\DataTableGateway;

interface SqlDataTableGateway extends DataTableGateway
{
    public static function types(array $state): array;

    /**
     * @return string
     */
    public function getDbName(): string;

    /**
     * @return Connection
     */
    public function getConnection(): Connection;
}
