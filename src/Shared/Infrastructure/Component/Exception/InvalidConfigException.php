<?php

namespace Zaioll\Shared\Infrastructure\Component\Exception;

use Exception;

class InvalidConfigException extends Exception
{
    public function getName(): string
    {
        return 'Invalid Configuration';
    }
}
