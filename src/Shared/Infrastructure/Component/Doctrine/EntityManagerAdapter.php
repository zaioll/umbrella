<?php

declare(strict_types=1);

namespace Zaioll\Shared\Infrastructure\Component\Doctrine;

use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\EntityManagerInterface as DoctrineEntityManagerInterface;
use Zaioll\Shared\Infrastructure\Component\ORM\EntityManagerInterface;

class EntityManagerAdapter implements EntityManagerInterface
{
    private $decorated;

    public function __construct(DoctrineEntityManagerInterface $em)
    {
        $this->decorated = $em;        
    }

    public function __get($name)
    {
        return $this->decorated->{$name};
    }

    public function __set($name, $value)
    {
        return $this->decorated->{$name} = $value;
    }

    public function __call($name, $arguments)
    {
        return $this->decorated->{$name}($arguments);
    }

    public static function __callStatic($name, $arguments)
    {
        return DoctrineEntityManagerInterface::{$name}($arguments);
    }

    /**
     * @inheritDoc
     */
    public function getConnection()
    {
        return $this->decorated->getConnection();
    }

    /**
     * @inheritDoc
     */
    public function getPartialReference($entityName, $identifier)
    {
        return $this->decorated->getPartialReference($entityName, $identifier);
    }

    /**
     * @inheritDoc
     */
    public function createNativeQuery($sql, ResultSetMapping $rsm)
    {
        return $this->decorated->createNamedNativeQuery($sql, $rsm);
    }

    /**
     * @inheritDoc
     */
    public function getReference($entityName, $id)
    {
        return $this->decorated->getReference($entityName, $id);
    }

    /**
     * @inheritDoc
     */
    public function copy($entity, $deep = false)
    {
        return $this->decorated->copy($entity, $deep);
    }

    /**
     * @inheritDoc
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        return $this->decorated->lock($entity, $lockMode, $lockVersion);
    }

    /**
     * @inheritDoc
     */
    public function commit()
    {
        return $this->decorated->commit();
    }

    /**
     * @inheritDoc
     */
    public function rollback()
    {
        return $this->decorated->rollback();
    }

    /**
     * @inheritDoc
     */
    public function wrapInTransaction(callable $func)
    {
        return $this->decorated->wrapInTransaction($func);
    }

    /**
     * @inheritDoc
     */
    public function transactional($func)
    {
        return $this->decorated->wrapInTransaction($func);
    }

    /**
     * @inheritDoc
     */
    public function createQuery($dql = '')
    {
        return $this->decorated->createQuery($dql);
    }


    /**
     * @inheritDoc
     */
    public function createNamedQuery($name)
    {
        return $this->decorated->createNamedQuery($name);
    }

    /**
     * @inheritDoc
     */
    public function createQueryBuilder()
    {
        return $this->decorated->createQueryBuilder();
    }

    /**
     * @inheritDoc
     */
    public function close()
    {
        return $this->decorated->close();
    }

    /**
     * @inheritDoc
     */
    public function getEventManager()
    {
        return $this->decorated->getEventManager();
    }

    /**
     * @inheritDoc
     */
    public function getConfiguration()
    {
        return $this->decorated->getConfiguration();
    }

    /**
     * @inheritDoc
     */
    public function isOpen()
    {
        return $this->decorated->isOpen();
    }

    /**
     * @inheritDoc
     */
    public function getUnitOfWork()
    {
        return $this->decorated->getUnitOfWork();
    }

    /**
     * @inheritDoc
     */
    public function createNamedNativeQuery($name)
    {
        return $this->decorated->createNamedNativeQuery($name);
    }

    /**
     * @inheritDoc
     **/ 
    public function getRepository($className)
    {
        return $this->decorated->getRepository($className);
    }

    /**
     * @inheritDoc
     */
    public function getCache()
    {
        return $this->getCache();
    }

    /**
     * @inheritDoc
     */
    public function getExpressionBuilder()
    {
        return $this->getExpressionBuilder();
    }

    /**
     * @inheritDoc
     */
    public function beginTransaction()
    {
        return $this->decorated->beginTransaction();
    }

    /**
     * @inheritDoc
     */
    public function newHydrator($hydrationMode)
    {
        return $this->decorated->newHydrator($hydrationMode);
    }

    /**
     * @inheritDoc
     */
    public function getProxyFactory()
    {
        return $this->decorated->getProxyFactory();
    }

    /**
     * @inheritDoc
     */
    public function getFilters()
    {
        return $this->decorated->getFilters();
    }

    /**
     * @inheritDoc
     */
    public function isFiltersStateClean()
    {
        return $this->decorated->isFiltersStateClean();
    }

    /**
     * @inheritDoc
     */
    public function hasFilters()
    {
        return $this->decorated->hasFilters();
    }

    /**
     * @inheritDoc
     */
    public function getClassMetadata($className)
    {
        return $this->decorated->getClassMetadata($className);
    }
}