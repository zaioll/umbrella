<?php

namespace Zaioll\Shared\Infrastructure\Helper;

use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;

/**
 * BaseStringHelper provides concrete implementation for [[StringHelper]].
 */
class StringHelper
{
    /**
     * Returns the number of bytes in the given string.
     * This method ensures the string is treated as a byte array by using `mb_strlen()`.
     * @param string $string the string being measured for length
     * @return int the number of bytes in the given string.
     */
    public static function byteLength(string $string): int
    {
        return mb_strlen($string, '8bit');
    }

    /**
     * Returns the portion of string specified by the start and length parameters.
     * This method ensures the string is treated as a byte array by using `mb_substr()`.
     * @param string $string the input string. Must be one character or longer.
     * @param int $start the starting position
     * @param int $length the desired portion length. If not specified or `null`, there will be
     * no limit on length i.e. the output will be until the end of the string.
     * @return string the extracted part of string, or FALSE on failure or an empty string.
     * @see https://secure.php.net/manual/en/function.substr.php
     */
    public static function byteSubstr(string $string, int $start, int $length = null): string
    {
        return mb_substr($string, $start, $length === null ? mb_strlen($string, '8bit') : $length, '8bit');
    }

    /**
     * Returns the trailing name component of a path.
     * This method is similar to the php function `basename()` except that it will
     * treat both \ and / as directory separators, independent of the operating system.
     * This method was mainly created to work on php namespaces. When working with real
     * file paths, php's `basename()` should work fine for you.
     * Note: this method is not aware of the actual filesystem, or path components such as "..".
     *
     * @param string $path A path string.
     * @param string $suffix If the name component ends in suffix this will also be cut off.
     * @return string the trailing name component of the given path.
     * @see https://secure.php.net/manual/en/function.basename.php
     */
    public static function basename(string $path, string $suffix = ''): string
    {
        if (($len = mb_strlen($suffix)) > 0 && mb_substr($path, -$len) === $suffix) {
            $path = mb_substr($path, 0, -$len);
        }
        $path = rtrim(str_replace('\\', '/', $path), '/\\');
        if (($pos = mb_strrpos($path, '/')) !== false) {
            return mb_substr($path, $pos + 1);
        }

        return $path;
    }

    /**
     * Returns parent directory's path.
     * This method is similar to `dirname()` except that it will treat
     * both \ and / as directory separators, independent of the operating system.
     *
     * @param string $path A path string.
     * @return string the parent directory's path.
     * @see https://secure.php.net/manual/en/function.basename.php
     */
    public static function dirname(string $path): string
    {
        $pos = mb_strrpos(str_replace('\\', '/', $path), '/');
        if ($pos !== false) {
            return mb_substr($path, 0, $pos);
        }

        return '';
    }

    /**
     * Truncates a string to the number of characters specified.
     *
     * @param string $string The string to truncate.
     * @param int $length How many characters from original string to include into truncated string.
     * @param string $suffix String to append to the end of truncated string.
     * @param string $encoding The charset to use, defaults to charset currently used by application.
     * @param bool $asHtml Whether to treat the string being truncated as HTML and preserve proper HTML tags.
     * This parameter is available since version 2.0.1.
     * @return string the truncated string.
     */
    public static function truncate(
        string $string,
        int $length,
        string $suffix = '...',
        string $encoding = 'UTF-8'
        //bool $asHtml = false
    ): string {
        /* @todo implementar depois de implementar middleware de view
        if ($asHtml) {
            return static::truncateHtml($string, $length, $suffix, $encoding);
        }
        */

        if (mb_strlen($string, $encoding) > $length) {
            return rtrim(mb_substr($string, 0, $length, $encoding)) . $suffix;
        }

        return $string;
    }

    /**
     * Truncates a string to the number of words specified.
     *
     * @param string $string The string to truncate.
     * @param int $count How many words from original string to include into truncated string.
     * @param string $suffix String to append to the end of truncated string.
     * @param bool $asHtml Whether to treat the string being truncated as HTML and preserve proper HTML tags.
     * This parameter is available since version 2.0.1.
     * @return string the truncated string.
     */
    public static function truncateWords(string $string, int $count, string $suffix = '...', bool $asHtml = false): string
    {
        if ($asHtml) {
            //return static::truncateHtml($string, $count, $suffix);
        }

        $words = preg_split('/(\s+)/u', trim($string), null, PREG_SPLIT_DELIM_CAPTURE);
        if (count($words) / 2 > $count) {
            return implode('', array_slice($words, 0, ($count * 2) - 1)) . $suffix;
        }

        return $string;
    }

    /**
     * Check if given string starts with specified substring.
     * Binary and multibyte safe.
     *
     * @param string $string Input string
     * @param string $with Part to search inside the $string
     * @param bool $caseSensitive Case sensitive search. Default is true. When case sensitive is enabled, $with must exactly match the starting of the string in order to get a true value.
     * @param string $encoding
     * @return bool Returns true if first input starts with second input, false otherwise
     */
    public static function startsWith($string, $with, $caseSensitive = true, string $encoding = 'UTF-8')
    {
        if (!$bytes = static::byteLength($with)) {
            return true;
        }
        if ($caseSensitive) {
            return strncmp($string, $with, $bytes) === 0;

        }
        return mb_strtolower(mb_substr($string, 0, $bytes, '8bit'), $encoding) === mb_strtolower($with, $encoding);
    }

    /**
     * Check if given string ends with specified substring.
     * Binary and multibyte safe.
     *
     * @param string $string Input string to check
     * @param string $with Part to search inside of the $string.
     * @param bool $caseSensitive Case sensitive search. Default is true. When case sensitive is enabled, $with must exactly match the ending of the string in order to get a true value.
     * @param string $encoding
     * @return bool Returns true if first input ends with second input, false otherwise
     */
    public static function endsWith($string, $with, $caseSensitive = true, string $encoding = 'UTF-8')
    {
        if (!$bytes = static::byteLength($with)) {
            return true;
        }
        if ($caseSensitive) {
            // Warning check, see https://secure.php.net/manual/en/function.substr-compare.php#refsect1-function.substr-compare-returnvalues
            if (static::byteLength($string) < $bytes) {
                return false;
            }

            return substr_compare($string, $with, -$bytes, $bytes) === 0;
        }

        return mb_strtolower(mb_substr($string, -$bytes, mb_strlen($string, '8bit'), '8bit'), $encoding) === mb_strtolower($with, $encoding);
    }

    /**
     * Explodes string into array, optionally trims values and skips empty ones.
     *
     * @param string $string String to be exploded.
     * @param string $delimiter Delimiter. Default is ','.
     * @param mixed $trim Whether to trim each element. Can be:
     *   - boolean - to trim normally;
     *   - string - custom characters to trim. Will be passed as a second argument to `trim()` function.
     *   - callable - will be called for each value instead of trim. Takes the only argument - value.
     * @param bool $skipEmpty Whether to skip empty strings between delimiters. Default is false.
     * @return array
     */
    public static function explode(string $string, string $delimiter = ',', $trim = true, bool $skipEmpty = false): array
    {
        $result = explode($delimiter, $string);
        if ($trim !== false) {
            if ($trim === true) {
                $trim = 'trim';
            } elseif (!is_callable($trim)) {
                $trim = function ($v) use ($trim) {
                    return trim($v, $trim);
                };
            }
            $result = array_map($trim, $result);
        }
        if ($skipEmpty) {
            // Wrapped with array_values to make array keys sequential after empty values removing
            $result = array_values(array_filter($result, function ($value) {
                return $value !== '';
            }));
        }

        return $result;
    }

    /**
     * Counts words in a string.
     *
     * @param string $string
     * @return int
     */
    public static function countWords($string): int
    {
        return count(preg_split('/\s+/u', $string, null, PREG_SPLIT_NO_EMPTY));
    }

    /**
     * Returns string representation of number value with replaced commas to dots, if decimal point
     * of current locale is comma.
     * @param int|float|string $value
     * @return string
     */
    public static function normalizeNumber($value)
    {
        $value = (string)$value;

        $localeInfo = localeconv();
        $decimalSeparator = isset($localeInfo['decimal_point']) ? $localeInfo['decimal_point'] : null;

        if ($decimalSeparator !== null && $decimalSeparator !== '.') {
            $value = str_replace($decimalSeparator, '.', $value);
        }

        return $value;
    }

    /**
     * Encodes string into "Base 64 Encoding with URL and Filename Safe Alphabet" (RFC 4648).
     *
     * > Note: Base 64 padding `=` may be at the end of the returned string.
     * > `=` is not transparent to URL encoding.
     *
     * @see https://tools.ietf.org/html/rfc4648#page-7
     * @param string $input the string to encode.
     * @return string encoded string.
     */
    public static function base64UrlEncode($input)
    {
        return strtr(self::base64Encode($input), '+/', '-_');
    }

    /**
     * Decodes "Base 64 Encoding with URL and Filename Safe Alphabet" (RFC 4648).
     *
     * @see https://tools.ietf.org/html/rfc4648#page-7
     * @param string $input encoded string.
     * @return string decoded string.
     */
    public static function base64UrlDecode($input)
    {
        return self::base64Decode(strtr($input, '-_', '+/'));
    }

    /**
     * Safely casts a float to string independent of the current locale.
     *
     * The decimal separator will always be `.`.
     * @param float|int $number a floating point number or integer.
     * @return string the string representation of the number.
     */
    public static function floatToString($number)
    {
        // . and , are the only decimal separators known in ICU data,
        // so its safe to call str_replace here
        return str_replace(',', '.', (string) $number);
    }

    /**
     * Checks if the passed string would match the given shell wildcard pattern.
     * This function emulates [[fnmatch()]], which may be unavailable at certain environment, using PCRE.
     * @param string $pattern the shell wildcard pattern.
     * @param string $string the tested string.
     * @param array $options options for matching. Valid options are:
     *
     * - caseSensitive: bool, whether pattern should be case sensitive. Defaults to `true`.
     * - escape: bool, whether backslash escaping is enabled. Defaults to `true`.
     * - filePath: bool, whether slashes in string only matches slashes in the given pattern. Defaults to `false`.
     *
     * @return bool whether the string matches pattern or not.
     */
    public static function matchWildcard($pattern, $string, $options = []): bool
    {
        if ($pattern === '*' && empty($options['filePath'])) {
            return true;
        }

        $replacements = [
            '\\\\\\\\' => '\\\\',
            '\\\\\\*' => '[*]',
            '\\\\\\?' => '[?]',
            '\*' => '.*',
            '\?' => '.',
            '\[\!' => '[^',
            '\[' => '[',
            '\]' => ']',
            '\-' => '-',
        ];

        if (isset($options['escape']) && !$options['escape']) {
            unset($replacements['\\\\\\\\']);
            unset($replacements['\\\\\\*']);
            unset($replacements['\\\\\\?']);
        }

        if (!empty($options['filePath'])) {
            $replacements['\*'] = '[^/\\\\]*';
            $replacements['\?'] = '[^/\\\\]';
        }

        $pattern = strtr(preg_quote($pattern, '#'), $replacements);
        $pattern = '#^' . $pattern . '$#us';

        if (isset($options['caseSensitive']) && !$options['caseSensitive']) {
            $pattern .= 'i';
        }

        return preg_match($pattern, $string) === 1;
    }

    /**
     * This method provides a unicode-safe implementation of built-in PHP function `ucfirst()`.
     *
     * @param string $string the string to be proceeded
     * @param string $encoding Optional, defaults to "UTF-8"
     * @return string
     * @see https://secure.php.net/manual/en/function.ucfirst.php
     */
    public static function mbUcfirst($string, $encoding = 'UTF-8'): string
    {
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $rest = mb_substr($string, 1, null, $encoding);

        return mb_strtoupper($firstChar, $encoding) . $rest;
    }

    /**
     * This method provides a unicode-safe implementation of built-in PHP function `ucwords()`.
     *
     * @param string $string the string to be proceeded
     * @param string $encoding Optional, defaults to "UTF-8"
     * @return string
     * @see https://secure.php.net/manual/en/function.ucwords.php
     */
    public static function mbUcwords($string, $encoding = 'UTF-8'): string
    {
        $words = preg_split("/\s/u", $string, -1, PREG_SPLIT_NO_EMPTY);

        $titelized = array_map(function ($word) use ($encoding) {
            return static::mbUcfirst($word, $encoding);
        }, $words);

        return implode(' ', $titelized);
    }

    /**
     * @param string $text
     *
     * @return string
     */
    public static function toSnakeCase(string $text): string
    {
        return ctype_lower($text) ? $text : strtolower(preg_replace('/([^A-Z\s])([A-Z])/', "$1_$2", $text));
    }

    /**
     * @param string $text
     *
     * @return string
     */
    public static function toCamelCase(string $text): string
    {
        return lcfirst(str_replace('_', '', ucwords($text, '_')));
    }

    /**
     * Remove caracteres diferentes alfanuméricos
     * @param string $text
     *
     * @return string
     */
    public static function clear(string $text): string
    {
        return preg_replace('/[^0-9a-zA-Z]+/', '', $text);
    }

    /**
     * Encapsula função nativa
     *
     * @link https://php.net/manual/en/function.base64-decode.php
     *
     * @param string $dados The encoded data.
     * @param bool $strict Returns false if input contains character from outside the base64 alphabet.
     *
     * @return string|false the original data or false on failure. The returned data may be binary.
     */
    public static function base64Decode(string $dados, bool $strict = false)
    {
        return base64_decode($dados, $strict);
    }

    /**
     * Encapsula função nativa
     *
     * @link https://php.net/manual/en/function.base64-encode.php
     *
     * @param string $dados  The data to encode.
     *
     * @return string The encoded data, as a string.
     */
    public static function base64Encode(string $dados): string
    {
        return base64_encode($dados);
    }

    /**
     * @param string $filename
     * @param bool $useIncludePath
     * @param resource $context
     * @param integer $offset
     * @param integer|null $length
     *
     * @return string|false
     *
     * @see https://www.php.net/manual/en/function.file-get-contents.php
     */
    public static function getFileContents(
        string $filename,
        bool $useIncludePath = true,
        $context = null,
        int $offset = 0,
        ?int $length = null
    ) {
        if ($length !== null) {
            return file_get_contents($filename, $useIncludePath, $context, $offset, $length);
        }
        return file_get_contents($filename, $useIncludePath, $context, $offset);
    }

    /**
     * @param string $string
     *
     * @return string
     *
     * @link https://php.net/manual/en/function.strtoupper.php
     */
    public static function toUpper(string $string): string
    {
        return strtoupper($string);
    }

    /**
     * @param string $string
     *
     * @return string
     *
     * @link https://php.net/manual/en/function.strtolower.php
     */
    public static function toLower(string $string): string
    {
        return strtolower($string);
    }

    /**
     * @param string $json
     * @param int $depth [optional]
     * @param int $flags [optional]
     *
     * Bitmask of JSON decode options:
     * {@see JSON_BIGINT_AS_STRING} decodes large integers as their original string value.
     * {@see JSON_INVALID_UTF8_IGNORE} ignores invalid UTF-8 characters,
     * {@see JSON_INVALID_UTF8_SUBSTITUTE} converts invalid UTF-8 characters to \0xfffd,
     * {@see JSON_OBJECT_AS_ARRAY} decodes JSON objects as PHP array, since 7.2.0 used by default if $assoc parameter is null,
     * {@see JSON_THROW_ON_ERROR} when passed this flag, the error behaviour of these functions is changed. The global error state is left untouched, and if an error occurs that would otherwise set it, these functions instead throw a JsonException
     * state is left untouched, and if an error occurs that would otherwise set it, these functions instead throw a JsonException
     *
     * @return mixed
     *
     * @link https://php.net/manual/en/function.json-decode.php
     */
    public static function jsonDecodeToAssociative(string $json, int $depth = 512, int $flags = JSON_HEX_QUOT)
    {
        return json_decode($json, true, $depth, $flags);
    }

    /**
     * @param string $json
     * @param int $depth [optional]
     * @param int $flags [optional]
     *
     * Bitmask of JSON decode options:
     * {@see JSON_BIGINT_AS_STRING} decodes large integers as their original string value.
     * {@see JSON_INVALID_UTF8_IGNORE} ignores invalid UTF-8 characters,
     * {@see JSON_INVALID_UTF8_SUBSTITUTE} converts invalid UTF-8 characters to \0xfffd,
     * {@see JSON_OBJECT_AS_ARRAY} decodes JSON objects as PHP array, since 7.2.0 used by default if $assoc parameter is null,
     * {@see JSON_THROW_ON_ERROR} when passed this flag, the error behaviour of these functions is changed. The global error state is left untouched, and if an error occurs that would otherwise set it, these functions instead throw a JsonException
     * state is left untouched, and if an error occurs that would otherwise set it, these functions instead throw a JsonException
     *
     * @return mixed
     *
     * @link https://php.net/manual/en/function.json-decode.php
     */
    public static function jsonDecode(string $json, int $depth = 512, int $flags = JSON_HEX_QUOT)
    {
        return json_decode($json, false, $depth, $flags);
    }

    /**
     * @param string $string
     * @return array
     */
    public static function wordsList(string $string): array
    {
        return str_word_count($string, 1, 'áàçúùüíìâêôãẽĩõũÁÀÇÚÙÍÌÂÊÔÃẼÕ');
    }

    /**
     * @param string $fullName
     *
     * @return array|string|false ['first', 'surname', 'last'], string when filter or false when fail
     */
    public static function parsePersonName(string $fullName, string $filter = 'full')
    {
        if (self::byteLength($fullName) === 0) {
            return false;
        }
        $words = self::wordsList($fullName);
        $qtd   = ArrayHelper::count($words);

        if ($qtd < 2) {
            return false;
        }
        $firstName = $words[0];
        if ($filter === 'first') {
            return $firstName;
        }
        $lastName  = $words[$qtd - 1];
        if ($filter === 'last') {
            return $lastName;
        }
        $surname   = '';
        for ($i = 1; $i < $qtd - 1; $i += 1) {
            if (self::byteLength($surname) === 0) {
                $surname .= $words[$i];
                continue;
            }
            $surname .= " {$words[$i]}";
        }
        if ($filter === 'surname') {
            return $surname;
        }
        return ['first' => $firstName, 'surname' => $surname, 'last' => $lastName];
    }
}
