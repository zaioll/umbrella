<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\ValueObject;

use Zaioll\Shared\Domain\ValueObject\BaseValueObject;

abstract class IntValueObject extends BaseValueObject
{
    /**
     * @var int $value
     */
    protected $value;

    public function __construct(int $value)
    {
        $this->value = $this->validateInput($value);
    }

    public function value(): int
    {
        return $this->value;
    }
}
