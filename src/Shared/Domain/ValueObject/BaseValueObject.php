<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\ValueObject;

use RuntimeException;
use Symfony\Component\Validator\Validation;
use Zaioll\Shared\Domain\ValueObject\ValueObject;
use Zaioll\Shared\Infrastructure\Helper\ArrayHelper;

abstract class BaseValueObject implements ValueObject
{
    protected function validateInput($value)
    {
        $violations = Validation::createValidator()->validate($value, self::getConstraints());
        if (ArrayHelper::count($violations) > 0) {
            $detail = $violations[0]->getMessage();
            throw new RuntimeException($detail . " Value: $value. Object: " . self::class);
        }

        return $value;
    }

    /**
     * @return mixed[]
     */
    public static function getConstraints(): array
    {
        return static::defineConstraints();
    }

    /**
     * @return mixed[]
     */
    abstract protected static function defineConstraints(): array;
}
