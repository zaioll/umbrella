<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\ValueObject;

use Zaioll\Shared\Domain\ValueObject\BaseValueObject;

abstract class StringValueObject extends BaseValueObject
{
    /**
     * @var string $value
     */
    protected $value;

    public function __construct(string $value)
    {
        $this->value = $this->validateInput($value);
    }

    public function value(): string
    {
        return $this->value;
    }
}
