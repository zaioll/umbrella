<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\ValueObject;

use Zaioll\Shared\Infrastructure\Component\Uid\UuidAdapter;
use Symfony\Component\Validator\Constraints as Assert;

abstract class UuidValueObject extends UuidAdapter
{
    private static $constrains;

    protected $uid;

    public function __construct()
    {
        $this->uid = self::uuid4();
    }

    /**
     * @return mixed[]
     */
    public static function getConstraints(): array
    {
        if (!isset(self::$constrains)) {
            self::$constrains = [new Assert\NotBlank(), new Assert\Uuid()];
        }
        return self::$constrains;
    }

    protected static function random(): self
    {
        return static::getFactory()->fromString(static::v4()->toRfc4122());
    }

    public function value(): string
    {
        return $this->uid->toString();
    }

    public function __toString(): string
    {
        return $this->value();
    }
}
