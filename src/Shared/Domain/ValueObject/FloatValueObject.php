<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\ValueObject;

use Zaioll\Shared\Domain\ValueObject\BaseValueObject;

abstract class FloatValueObject extends BaseValueObject
{
    /**
     * @var float $value
     */
    protected $value;

    /**
     * @param string|float $value
     */
    public function __construct($value)
    {
        $this->value = $this->validateInput($value);
    }

    public function value(): float
    {
        return $this->value;
    }
}
