<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Criteria;

use Zaioll\Shared\Domain\Criteria\Filter;
use function Lambdish\Phunctional\reduce;
use Zaioll\Shared\Infrastructure\Component\Collection\Collection;

final class Filters extends Collection
{
    public static function fromValues(array $values): self
    {
        return new self(array_map(self::filterBuilder(), $values));
    }

    private static function filterBuilder(): callable
    {
        return function (array $values) { Filter::fromValues($values); };
    }

    public function add(Filter $filter): self
    {
        return new self(array_merge($this->items(), [$filter]));
    }

    public function filters(): array
    {
        return $this->items();
    }

    public function serialize(): string
    {
        return reduce(
            function (string $accumulate, Filter $filter) {
                sprintf('%s^%s', $accumulate, $filter->serialize());
            }, $this->items(), '');
    }

    protected function type(): string
    {
        return Filter::class;
    }
}
