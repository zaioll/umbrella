<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Bus\Event;

use Zaioll\Shared\Infrastructure\Helper\DateTimeHelper;
use Zaioll\Shared\Infrastructure\Component\Uid\UuidAdapter;

abstract class DomainEvent
{
    public static $dateFormat = 'Y-m-d H:i:s';
    private $eventId;
    private $eventDate;

    private $aggregateId;

    public function __construct(string $aggregateId, string $eventId = null, string $eventDate = null)
    {
        $this->aggregateId = $aggregateId;
        $this->eventId   = $eventId ?: UuidAdapter::uuid4()->toString();
        $this->eventDate = $eventDate ?: (DateTimeHelper::agoraUtc())->format(self::$dateFormat);
    }

    /**
     * @param string $aggregateId
     * @param array $data
     * @param string $eventId
     * @param string $eventDate
     *
     * @return self
     */
    abstract public static function fromPrimitives(
        string $aggregateId,
        array $data,
        string $eventId = null,
        string $eventDate = null
    ): self;

    /**
     * @return mixed[]
     */
    abstract public function toPrimitives(): array;

    public function aggregateId(): string
    {
        return $this->aggregateId;
    }

    public function eventId(): string
    {
        return $this->eventId;
    }

    public function occurredOn(): string
    {
        return $this->eventDate;
    }
}
