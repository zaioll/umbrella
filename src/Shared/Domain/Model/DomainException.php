<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Model;

use Exception;

class DomainException extends Exception
{
}
