<?php

declare(strict_types=1);

namespace Zaioll\Shared\Domain\Model;

interface EventAble
{
    public function dropEvents(): array;
}
